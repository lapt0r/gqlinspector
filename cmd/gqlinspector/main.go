package main

import (
	"os"

	cli "gitlab.com/lapt0r/gqlinspector/cli"
)

func main() {
	os.Exit(cli.Run(os.Args[1:]))
}
