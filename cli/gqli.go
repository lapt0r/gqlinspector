package cli

import (
	"bytes"
	"encoding/json"
	"flag"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"net/url"
	"strings"

	gqli "gitlab.com/lapt0r/gqlinspector/inspector"
)

const (
	shellOK = iota
	shellGenericError
	shellParseError
)

//Run runs the CLI implementation of GQL Inspector
func Run(args []string) int {
	if err := runInternal(args); err != nil {
		if err, ok := err.(interface{ ExitCode() int }); ok {
			return err.ExitCode()
		}
		return shellGenericError
	}
	return shellOK
}

func runInternal(args []string) (err error) {
	var targetUrl string
	flag.StringVar(&targetUrl, "url", "", "The GraphQL url to scan")
	flag.Parse()

	proxyUrl, err := url.Parse("http://localhost:1337")
	http.DefaultTransport = &http.Transport{Proxy: http.ProxyURL(proxyUrl)}

	json, err := json.Marshal(gqli.ApolloIntrospectionQuery)
	if err != nil {
		return err
	}
	payload := bytes.NewBuffer(json)
	resp, err := http.Post(targetUrl, "application/json", payload)
	if err != nil {
		return err
	}

	log.Printf("Response from server %v was %v", targetUrl, resp.StatusCode)
	bytes, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return err
	}
	queries := gqli.GetQueries(bytes)
	if err != nil {
		return err
	}
	println("Queries found:")
	println("--------")
	writeOutput(queries, bytes)

	mutations := gqli.GetMutations(bytes)
	if err != nil {
		return err
	}
	println("Mutations found:")
	println("--------")
	writeOutput(mutations, bytes)
	return nil
}

func writeOutput(operations []string, bytes []byte) {
	for _, item := range operations {
		println(item)
		args := gqli.GetArguments(bytes, item)
		var sb strings.Builder
		sb.WriteString(fmt.Sprintf("query q{ %v(", item))
		argCount := len(args)
		for idx, arg := range args {
			sb.WriteString(arg)
			println(fmt.Sprintf("\t%v", arg))
			if idx < argCount-1 {
				sb.WriteString(", ")
			}
		}
		sb.WriteString("){")
		//todo: return type parsing
		sb.WriteString("}}")
		println(sb.String())
	}
}
