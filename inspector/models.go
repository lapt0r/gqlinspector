package inspector

//GqlQuery is the struct for Apollo-compliant POST queries
type GqlQuery struct {
	OperationName string   `json:"operationName"`
	Variables     []string `json:"variables"`
	Query         string   `json:"query"`
}

//MinimalGqlQuery sends the minimum necessary introspection query
type MinimalGqlQuery struct {
	Query string `json:"query"`
}

const introspectionQueryString = "query IntrospectionQuery { __schema { queryType { name } mutationType { name } subscriptionType { name } types { ...FullType } directives { name description locations args { ...InputValue } } } } fragment FullType on __Type { kind name description fields(includeDeprecated: true) { name description args { ...InputValue } type { ...TypeRef } isDeprecated deprecationReason } inputFields { ...InputValue } interfaces { ...TypeRef } enumValues(includeDeprecated: true) { name description isDeprecated deprecationReason } possibleTypes { ...TypeRef } } fragment InputValue on __InputValue { name description type { ...TypeRef } defaultValue } fragment TypeRef on __Type { kind name ofType { kind name ofType { kind name ofType { kind name ofType { kind name ofType { kind name ofType { kind name ofType { kind name } } } } } } } }"

//ApolloIntrospectionQuery - Apollo-compliant GraphQL query request object for marshaling
var ApolloIntrospectionQuery = GqlQuery{
	OperationName: "IntrospectionQuery",
	Variables:     make([]string, 0),
	Query:         introspectionQueryString,
}

//GenericIntrospectionQuery - minimal query that passes only the 'query' parameter
var GenericIntrospectionQuery = GqlQuery{
	Query: introspectionQueryString,
}
