package inspector

import (
	"encoding/json"
	"fmt"
	"log"

	"github.com/itchyny/gojq"
)

var gqlPrimitiveTypes = map[string]struct{}{"Int": {}, "Boolean": {}, "String": {}, "ID": {}}

//GetQueries gets the queries from an introspection result
func GetQueries(input []byte) []string {
	//this will always return a single result
	queryName := jqInternal(input, ".data.__schema.queryType.name")[0]
	return jqInternal(input, fmt.Sprintf(".data.__schema.types[] | select(.name==\"%v\") | .fields[].name", queryName))
}

//GetMutations gets the mutations from an introspection result
func GetMutations(input []byte) []string {
	//this will always return a single result
	mutationName := jqInternal(input, ".data.__schema.mutationType.name")[0]
	return jqInternal(input, fmt.Sprintf(".data.__schema.types[] | select(.name==\"%v\") | .fields[].name", mutationName))
}

//GetArguments gets the arguments from an query or mutation
func GetArguments(input []byte, operation string) []string {
	nameQueryString := fmt.Sprintf(".data.__schema.types[] | select(.name==\"Query\" or .name ==\"Mutation\") | .fields[] | select(.name == \"%v\") | .args[].name", operation)
	names := jqInternal(input, nameQueryString)
	typeQueryString := fmt.Sprintf(".data.__schema.types[] | select(.name==\"Query\" or .name ==\"Mutation\") | .fields[] | select(.name == \"%v\") | .args[].type.ofType.name", operation)
	types := jqInternal(input, typeQueryString)
	result := make([]string, 0)
	if len(names) != len(types) {
		//shouldn't happen, every arg name should have a type
		log.Fatalln("Arg name/type length mismatch")
	}

	for idx, name := range names {
		result = append(result, fmt.Sprintf("%v : %v", name, types[idx]))
	}
	return result
}

func getTypeInfo(input []byte, typeString string) []string {
	return make([]string, 0)
}

func isPrimitiveType(typeString string) bool {
	_, isPrimitive := gqlPrimitiveTypes[typeString]
	return isPrimitive
}

func jqInternal(input []byte, queryString string) []string {
	query, err := gojq.Parse(queryString)
	if err != nil {
		log.Fatalln(err)
	}

	//optimization todo: initialization
	result := make([]string, 0)
	marshaledObject := make(map[string]interface{})
	json.Unmarshal(input, &marshaledObject)
	iter := query.Run(marshaledObject)
	for {
		item, ok := iter.Next()
		if !ok {
			break
		}
		if err, ok := item.(error); ok {
			log.Fatalln(err)
		}
		result = append(result, fmt.Sprintf("%v", item))
	}
	return result
}
